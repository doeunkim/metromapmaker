/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MMM.tran;

import MMM.data.DraggableEllipse;
import MMM.data.MMMData;
import jtps.jTPS_Transaction;

/**
 *
 * @author kse
 */
public class AddStationTran implements jTPS_Transaction{
     private MMMData data;
    private DraggableEllipse station;
    public AddStationTran(MMMData initData, DraggableEllipse initStation){
        data = initData;
        station = initStation;
    }
    @Override
    public void doTransaction(){
         data.addNewStation(station);
    }
    @Override
    public void undoTransaction(){
        //data.removeStation(station);
    }
}
