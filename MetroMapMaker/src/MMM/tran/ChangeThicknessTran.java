/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MMM.tran;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import jtps.jTPS_Transaction;
/**
 *
 * @author Doeun Kim
 */
public class ChangeThicknessTran implements jTPS_Transaction{
    private Shape shape;
    private double outlineThickness;
    private double oldOutlineThickness;
    
    public ChangeThicknessTran(Shape initShape, double initOutlineThickness) {
        shape = initShape;
        outlineThickness = initOutlineThickness;
        oldOutlineThickness = shape.getStrokeWidth();
    }

    @Override
    public void doTransaction() {
        shape.setStrokeWidth(outlineThickness);
    }

    @Override
    public void undoTransaction() {
        shape.setStrokeWidth(oldOutlineThickness);
    }    
}
