/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MMM.tran;

import MMM.data.MMMData;
import MMM.data.MetroLine;
import jtps.jTPS_Transaction;

/**
 *
 * @author kse
 */
public class AddLineTran implements jTPS_Transaction{
     private MMMData data;
    private MetroLine line;
    public AddLineTran(MMMData initData, MetroLine initLine){
        data = initData;
        line = initLine;
    }
    @Override
    public void doTransaction(){
         data.addLine(line);
    }
    @Override
    public void undoTransaction(){
        //data.removeLine(line);
    }
}
