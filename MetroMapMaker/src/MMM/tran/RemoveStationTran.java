/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MMM.tran;

import MMM.data.DraggableEllipse;
import MMM.data.DraggableText;
import MMM.data.MMMData;
import jtps.jTPS_Transaction;

/**
 *
 * @author kse
 */
public class RemoveStationTran implements jTPS_Transaction{
     private MMMData data;
    private DraggableEllipse station;
    private DraggableText label;
    public RemoveStationTran(MMMData initData, DraggableEllipse initStation, DraggableText initStationLabel){
        data = initData;
        station = initStation;
        label = initStationLabel;
    }
    @Override
    public void doTransaction(){
         data.removeStation(station, label);
         
    }
    @Override
    public void undoTransaction(){
        data.setSelectedNode(station);
        data.highlightNode(station);
        data.addStation(station);
        //data.getConnections().put(station, new ArrayList<DraggableEllipse>());
    }
}
