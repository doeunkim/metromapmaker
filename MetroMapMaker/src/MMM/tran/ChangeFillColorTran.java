/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MMM.tran;

import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import jtps.jTPS_Transaction;

/**
 *
 * @author Doeun Kim
 */
public class ChangeFillColorTran implements jTPS_Transaction {
    private Shape shape;
    private Color color;
    private Color oldColor;
    
    public ChangeFillColorTran(Shape initShape, Color initColor) {
        shape = initShape;
        color = initColor;
        oldColor = (Color)shape.getFill();
    }

    @Override
    public void doTransaction() {
        shape.setFill(color);
    }

    @Override
    public void undoTransaction() {
        shape.setFill(oldColor);
    }    
}