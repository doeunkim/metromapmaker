/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MMM.tran;

import MMM.data.MMMData;
import java.util.ArrayList;
import javafx.scene.shape.Line;
import jtps.jTPS_Transaction;

/**
 *
 * @author kse
 */
public class RemoveLineTran implements jTPS_Transaction{
     private MMMData data;
    private ArrayList<Line> lineSegs;
    public RemoveLineTran(MMMData initData, ArrayList<Line> initLine){
        data = initData;
        lineSegs = initLine;
    }
    @Override
    public void doTransaction(){
         data.removeLine(lineSegs);
    }
    @Override
    public void undoTransaction(){
//        ObservableList<Node> newList =data.getNodes(); 
//        newList.add(data.getNodes().indexOf(line), line);
//        data.setNodes(newList);
//        data.setSelectedNode(line);
//        data.highlightNode(line);
//       data.setMetroLineIndex(data.getMetroLineIndex()+1);
//        data.getLines().add(line);
    }
}
