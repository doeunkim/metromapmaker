package MMM;

/**
 * This class provides the properties that are needed to be loaded from
 * language-dependent XML files.
 * 
 * @author Doeun Kim
 * @version 1.0
 */
public enum MMMLanguageProperty {
    LIST_ICON,
    ROTATE_ICON,
    ROUTE_ICON,
    MINUS_ICON,
    PLUS_ICON,
    ZOOMOUT_ICON,
    ZOOMIN_ICON,
    INCREASE_ICON,
    DECREASE_ICON,
    ITALIC_ICON,
    BOLD_ICON,
    EXPORT_ICON,
    SAVEAS_ICON,
    UNDO_ICON,
    REDO_ICON,
    ABOUT_ICON,
    ABOUT_TOOLTIP,
    REDO_TOOLTIP,
    UNDO_TOOLTIP,
    EXPORT_TOOLTIP,
    SAVEAS_TOOLTIP,
    ZOOMIN_TOOLTIP,
    ZOOMOUT_TOOLTIP,
    DECREASE_TOOLTIP,
    INCREASE_TOOLTIP,
    BOLD_TOOLTIP,
    ITALIC_TOOLTIP,
    ROUTE_TOOLTIP,
    LIST_TOOLTIP,
    ROTATE_TOOLTIP,
    /* DEFAULT INITIAL POSITIONS WHEN ADDING IMAGES AND TEXT */
    DEFAULT_NODE_X,
    DEFAULT_NODE_Y
}