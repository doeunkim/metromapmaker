package MMM.gui;

import MMM.data.Draggable;
import javafx.scene.Scene;
import MMM.data.MMMData;
import MMM.data.DraggableEllipse;
import MMM.data.DraggableImage;
import MMM.data.DraggableText;
import MMM.data.MMMState;
import MMM.data.MetroLine;
import djf.AppTemplate;
import java.util.ArrayList;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

/**
 * This class responds to interactions with the rendering surface.
 *
 * @author Doeun Kim
 * @version 1.0
 */
public class MMMCanvasController {

    AppTemplate app;
    Line lineToMove;
    MetroLine metroLineToMove;
    double oldX;
    double oldY;
    int startX;
    int startY;

    public MMMCanvasController(AppTemplate initApp) {
        app = initApp;
    }

    private MetroLine findSelectedLine(String lineName) {
        for (MetroLine node : ((MMMData) app.getDataComponent()).getLines()) {
            if (node instanceof MetroLine && node.getId().equals(lineName)) {
                return node;
            }
        }
        return null;
    }

    private int findIndexOfClosestStation(Node station, Node line) {
        int index = -1;
        double x = ((DraggableEllipse) station).getCenterX();
        double y = ((DraggableEllipse) station).getCenterY();
        double minDis = 100000000;
        ArrayList<DraggableEllipse> stations = ((MetroLine) line).getStations();
        for (int i = 0; i < stations.size(); i++) {
            double x1 = stations.get(i).getCenterX();
            double y1 = stations.get(i).getCenterY();
            double dis = findPythagorianDistance(x, y, x1, y1);
            if (minDis > dis) {
                minDis = dis;
                index = i;
            }
        }
        return index;
    }

    private double findPythagorianDistance(double x, double y, double x1, double y1) {
        return Math.sqrt(Math.pow(x - x1, 2) + Math.pow(y - y1, 2));
    }

    private Line createRightLineSegment(MetroLine metroLine, DraggableEllipse node, int index) {
        Line leftLine = metroLine.getLineSegments().get(index);
        // GET OLD X,Y
        double oldendx = leftLine.getEndX();
        double oldendy = leftLine.getEndY();
        // LEFT LINE SEGMENT'S END X,Y COORDINATE BINDS WITH THE STATION'S CENTER X,Y COORDINATE
        leftLine.endXProperty().bind(node.centerXProperty());
        leftLine.endYProperty().bind(node.centerYProperty());
        // CREATE RIGHT LINE SEGMENT
        Line rightLine = new Line();
        rightLine.setStroke((Color) leftLine.getStroke());
        rightLine.setStrokeWidth(leftLine.getStrokeWidth());
        rightLine.setId(leftLine.getId());
        rightLine.setOpacity(1.0);
        // RIGHT LINE SEGMENT'S START X,Y COORDINATE BINDS WITH THE STATION'S CENTER X,Y COORDINATE
        rightLine.startXProperty().bind(node.centerXProperty());
        rightLine.startYProperty().bind(node.centerYProperty());
        // SET RIGHT LINE SEGMENT'S END X,Y COORDINATE
        rightLine.setEndX(oldendx);
        rightLine.setEndY(oldendy);
        return rightLine;
    }

    private void addStationToRightEnd(MetroLine metroLine, Line rightLine, DraggableEllipse node, int indexToTheClosestStation, MMMData dataManager, String selectedLineName) {
        // ADD RIGHT LINE SEGMENT TO THE OBSERVABLE LIST
        dataManager.getNodes().add(0, rightLine);
        // ADD RIGHT LINE SEGMENT TO THE ARRAY LIST OF METRO LINE
        metroLine.getLineSegments().add(indexToTheClosestStation + 2, rightLine);
        // MOVE RIGHT LABEL
        for (Node n : dataManager.getNodes()) {
            if (n instanceof DraggableText && ((DraggableText) n).getTextAlignment().equals(TextAlignment.LEFT) && ((DraggableText) n).getText().equals(selectedLineName)) {
                ((DraggableText) n).xProperty().bind(metroLine.getLineSegments().get(metroLine.getLineSegments().size() - 1).endXProperty());
                ((DraggableText) n).yProperty().bind(metroLine.getLineSegments().get(metroLine.getLineSegments().size() - 1).endYProperty());
                break;
            }
        }
        // ADD THE STATION TO THE TWO ARRAY LISTS OF METROLINE
        metroLine.getStationNames().add(indexToTheClosestStation + 1, node.getId());
        metroLine.getStations().add(indexToTheClosestStation + 1, (DraggableEllipse) node);
        ((DraggableEllipse) node).getLines().add(selectedLineName);
    }

    private void addStationToLeftEnd(MetroLine metroLine, Line rightLine, DraggableEllipse node, int indexToTheClosestStation, MMMData dataManager, String selectedLineName) {
        // ADD RIGHT LINE SEGMENT TO THE OBSERVABLE LIST
        dataManager.getNodes().add(0, rightLine);
        // ADD RIGHT LINE SEGMENT TO THE ARRAY LIST OF METRO LINE
        metroLine.getLineSegments().add(indexToTheClosestStation + 1, rightLine);
        // MOVE LEFT LABEL
        for (Node n : dataManager.getNodes()) {
            if (n instanceof DraggableText && ((DraggableText) n).getTextAlignment().equals(TextAlignment.RIGHT) && ((DraggableText) n).getText().equals(selectedLineName)) {
                ((DraggableText) n).xProperty().bind(metroLine.getLineSegments().get(0).startXProperty().subtract(((DraggableText) n).getLayoutBounds().getWidth()));
                ((DraggableText) n).yProperty().bind(metroLine.getLineSegments().get(0).startYProperty());
                break;
            }
        }
        // CONNECT RIGHT LINE WITH ANOTHER STATION
        rightLine.endXProperty().bind(metroLine.getStations().get(0).centerXProperty());
        rightLine.endYProperty().bind(metroLine.getStations().get(0).centerYProperty());
        // ADD THE STATION TO THE TWO ARRAY LISTS OF METROLINE
        if (!metroLine.getStationNames().contains(node.getId())) {
            metroLine.getStationNames().add(0, node.getId());
            metroLine.getStations().add(0, (DraggableEllipse) node);
        }

        ((DraggableEllipse) node).getLines().add(selectedLineName);
    }

    private void addStationBetweenTwoStations(MetroLine metroLine, Line rightLine, DraggableEllipse node,DraggableEllipse rightnode, int indexToTheClosestStation, MMMData dataManager, String selectedLineName) {
        // ADD RIGHT LINE SEGMENT TO THE OBSERVABLE LIST
        dataManager.getNodes().add(0, rightLine);
        // ADD RIGHT LINE SEGMENT TO THE ARRAY LIST OF METRO LINE
        metroLine.getLineSegments().add(indexToTheClosestStation + 1, rightLine);
        // CONNECT LEFT LINE WITH LEFT STATION
//        metroLine.getLineSegments().get(indexToTheClosestStation).endXProperty().bind(node.centerXProperty());
//        metroLine.getLineSegments().get(indexToTheClosestStation).endYProperty().bind(node.centerYProperty());
        // CONNECT RIGHT LINE WITH RIGHT STATION
        rightLine.endXProperty().bind(metroLine.getStations().get(metroLine.getStations().indexOf(rightnode)).centerXProperty());
        rightLine.endYProperty().bind(metroLine.getStations().get(metroLine.getStations().indexOf(rightnode)).centerYProperty());
        // ADD THE STATION TO THE TWO ARRAY LISTS OF METROLINE
        if (!metroLine.getStationNames().contains(node.getId())) {
            metroLine.getStationNames().add(indexToTheClosestStation, node.getId());
            metroLine.getStations().add(indexToTheClosestStation, node);
        }
        node.getLines().add(selectedLineName);
    }

    /**
     * Respond to mouse presses on the rendering surface, which we call canvas,
     * but is actually a Pane.
     */
    public void processCanvasMousePress(int x, int y) {
        MMMData dataManager = (MMMData) app.getDataComponent();
        MMMWorkspace workspace = (MMMWorkspace) app.getWorkspaceComponent();
        Scene scene = app.getGUI().getPrimaryScene();
        Node node = dataManager.selectTopNode(x, y);
        if (node == null) {
            // MAKE ALL NODES UNHIGHLIGHTED
            for (Node n : dataManager.getNodes()) {
                dataManager.unhighlightNode(n);
            }
            if (dataManager.isInState(MMMState.ADDING_STATION)) {
                workspace.getAddStationToLine().setSelected(false);
                scene.setCursor(Cursor.DEFAULT);
                dataManager.setState(MMMState.DOING_NOTHING);
            } else if (dataManager.isInState(MMMState.REMOVING_STATION)) {
                workspace.getRemoveStationFromLine().setSelected(false);
                scene.setCursor(Cursor.DEFAULT);
                dataManager.setState(MMMState.DOING_NOTHING);
            }
            workspace.getMetroStationColorPicker().setValue(Color.BLACK);
        } else {
            dataManager.highlightNode(node);
            dataManager.setSelectedNode(node);
            // ADD STATIONS TO LINE
            if (dataManager.isInState(MMMState.ADDING_STATION)) {
                // CLICK ON THE STATION
                if (node instanceof DraggableEllipse) {
                    DraggableEllipse station = (DraggableEllipse) node;
                    String selectedLineName = workspace.getMetroLines().getValue().toString();
                    // FIND THE METRO LINE
                    MetroLine metroLine = null;
                    for (MetroLine ml : dataManager.getLines()) {
                        if (ml.getId().equals(selectedLineName)) {
                            metroLine = ml;
                            break;
                        }
                    }
                    // IF THE METRO LINE HAS NO STATION
                    if (metroLine.getStations().size() == 0) {
                        Line rightLine = createRightLineSegment(metroLine, station, 0);
                        // ADD RIGHT LINE SEGMENT TO THE OBSERVABLE LIST
                        dataManager.getNodes().add(0, rightLine);
                        // ADD RIGHT LINE SEGMENT TO THE ARRAY LIST OF METRO LINE
                        metroLine.getLineSegments().add(rightLine);
                        // MOVE RIGHT LABEL
                        for (Node n : dataManager.getNodes()) {
                            if (n instanceof DraggableText && ((DraggableText) n).getTextAlignment().equals(TextAlignment.LEFT) && ((DraggableText) n).getText().equals(selectedLineName)) {
                                ((DraggableText) n).xProperty().bind(metroLine.getLineSegments().get(metroLine.getLineSegments().size() - 1).endXProperty().add(20));
                                ((DraggableText) n).yProperty().bind(metroLine.getLineSegments().get(metroLine.getLineSegments().size() - 1).endYProperty());
                                break;
                            }
                        }
                        // ADD THE STATION TO THE TWO ARRAY LISTS OF METROLINE
                        metroLine.getStationNames().add(station.getId());
                        metroLine.getStations().add(station);
                        station.getLines().add(selectedLineName);
                    } else {
                        MetroLine line = findSelectedLine(selectedLineName);
                        // FIND THE INDEX OF THE CLOSEST STATION ON THE LINE
                        int indexOfTheClosestStation = findIndexOfClosestStation(station, line);
                        // IF THERE IS ONLY ONE STATION ON THE LINE
                        if (line.getStationNames().size() == 1) {
                            // IF THE STATION TO ADD IS ON THE LEFT OF THE CLOSEST STATION
                            if (findPythagorianDistance(station.getCenterX(), station.getCenterY(), line.getLineSegments().get(1).getEndX(), line.getLineSegments().get(1).getEndY())
                                    > findPythagorianDistance(station.getCenterX(), station.getCenterY(), line.getLineSegments().get(0).getStartX(), line.getLineSegments().get(0).getStartY())) {
                                Line rightLine = createRightLineSegment(metroLine, station, indexOfTheClosestStation);
                                addStationToLeftEnd(metroLine, rightLine, station, 0, dataManager, selectedLineName);
                            } // IF THE STATION TO ADD IS ON THE RIGHT OF THE CLOSEST STATION
                            else {
                                // CREATE RIGHT LINE SEGMENT
                                Line rightLine = createRightLineSegment(metroLine, station, 1);
                                addStationToRightEnd(metroLine, rightLine, station, 0, dataManager, selectedLineName);
                            }
                        } // IF THERE ARE MORE THAN ONE STATIONS ON THE LINE
                        else {
                            // INDEX OF THE CLOSEST STATION IS THE LAST STATION
                            if (indexOfTheClosestStation == metroLine.getStations().size() - 1) {
                                // STATION IS ON THE LEFT OF THE LAST STATION
                                if (findPythagorianDistance(station.getCenterX(), station.getCenterY(), line.getLineSegments().get(line.getLineSegments().size() - 1).getEndX(), line.getLineSegments().get(line.getLineSegments().size() - 1).getEndY())
                                        > findPythagorianDistance(station.getCenterX(), station.getCenterY(), line.getStations().get(indexOfTheClosestStation - 1).getCenterX(), line.getStations().get(indexOfTheClosestStation - 1).getCenterY())) {
                                    Line rightLine = createRightLineSegment(metroLine, station, indexOfTheClosestStation);
                                    addStationBetweenTwoStations(metroLine, rightLine, station,  metroLine.getStations().get(indexOfTheClosestStation), indexOfTheClosestStation, dataManager, selectedLineName);

                                } // STATION IS ON THE RIGHT OF THE LAST STATION
                                else {
                                    Line rightLine = createRightLineSegment(metroLine, station, metroLine.getLineSegments().size() - 1);
                                    addStationToRightEnd(metroLine, rightLine, station, indexOfTheClosestStation, dataManager, selectedLineName);

                                }
                            } // INDEX OF THE CLOSEST STATION IS THE FIRST STATION
                            else if (indexOfTheClosestStation == 0) {
                                // STATION IS ON THE LEFT OF THE FIRST STATION
                                if (findPythagorianDistance(station.getCenterX(), station.getCenterY(), line.getLineSegments().get(0).getStartX(), line.getLineSegments().get(0).getStartY())
                                        < findPythagorianDistance(station.getCenterX(), station.getCenterY(), line.getStations().get(indexOfTheClosestStation + 1).getCenterX(), line.getStations().get(indexOfTheClosestStation + 1).getCenterY())) {
                                    Line rightLine = createRightLineSegment(metroLine, station, 0);
                                    addStationToLeftEnd(metroLine, rightLine, station, 0, dataManager, selectedLineName);
                                } // STATION IS ON THE RIGHT OF THE FIRST STATION
                                else {
                                    Line rightLine = createRightLineSegment(metroLine, station, 1);
                                    addStationBetweenTwoStations(metroLine, rightLine, station, metroLine.getStations().get(indexOfTheClosestStation + 1), indexOfTheClosestStation + 1, dataManager, selectedLineName);
                                }
                            } // INDEX OF THE CLOSEST STATION IS SOMEWHERE IN THE MIDDLE
                            else {
                                Line rightLine = createRightLineSegment(metroLine, station, indexOfTheClosestStation);
                                addStationBetweenTwoStations(metroLine, rightLine, station, metroLine.getStations().get(indexOfTheClosestStation), indexOfTheClosestStation, dataManager, selectedLineName);
                            }
                        }
                    }
                }
            } // REMOVE STATIONS FROM LINE
            else if (dataManager.isInState(MMMState.REMOVING_STATION)) {
                // CLICK ON THE STATION
                if (node instanceof DraggableEllipse) {
                    DraggableEllipse station = (DraggableEllipse) node;
                    String selectedLine = workspace.getMetroLines().getValue().toString();
                    // IF THE STATION IS ON THE SELECTED LINE
                    if (station.getLines().contains(selectedLine)) {
                        // REMOVE LINE FROM STATION'S LINE LIST
                        station.getLines().remove(selectedLine);
                        // FIND THE METRO LINE
                        MetroLine metroLine = null;
                        for (MetroLine ml : dataManager.getLines()) {
                            if (ml.getId().equals(selectedLine)) {
                                metroLine = ml;
                                break;
                            }
                        }
                        // GET INDEX OF STATION
                        int index = metroLine.getStations().indexOf(station);
                        // STATION IS ON THE RIGHT END
                        if (index == metroLine.getStations().size() - 1) {
                            double oldendx = metroLine.getLineSegments().get(index + 1).getEndX();
                            double oldendy = metroLine.getLineSegments().get(index + 1).getEndY();
                            // REMOVE RIGHT  LINE
                            dataManager.getNodes().remove(metroLine.getLineSegments().get(index + 1));
                            metroLine.getLineSegments().remove(index + 1);
                            // REMOVE STATION
                            metroLine.removeStationName(station.getId());
                            metroLine.removeStation(station);
                            // FIND THE RIGHT LABEL
                            DraggableText rightLabel = null;
                            for (Node n : dataManager.getNodes()) {
                                if (n instanceof DraggableText && ((DraggableText) n).getText().equals(selectedLine) && ((DraggableText) n).getTextAlignment().equals(TextAlignment.LEFT)) {
                                    rightLabel = (DraggableText) n;
                                    break;
                                }
                            }
                            // RELOCATE LEFT LINE
                            metroLine.getLineSegments().get(index).endXProperty().unbind();
                            metroLine.getLineSegments().get(index).endYProperty().unbind();
                            metroLine.getLineSegments().get(index).endXProperty().set(oldendx);
                            metroLine.getLineSegments().get(index).endYProperty().set(oldendy);
                            rightLabel.xProperty().bind(metroLine.getLineSegments().get(index).endXProperty());
                            rightLabel.yProperty().bind(metroLine.getLineSegments().get(index).endYProperty());
                        } 
                        // STATION IS ON THE LEFT END OR THE MID
                        else {
                            metroLine.getLineSegments().get(index).endXProperty().bind(metroLine.getStations().get(index + 1).centerXProperty());
                            metroLine.getLineSegments().get(index).endYProperty().bind(metroLine.getStations().get(index + 1).centerYProperty());
                            // REMOVE RIGHT  LINE
                            dataManager.getNodes().remove(metroLine.getLineSegments().get(index + 1));
                            metroLine.getLineSegments().remove(index + 1);
                            // REMOVE STATION
                            metroLine.removeStationName(station.getId());
                            metroLine.removeStation(station);
                        }
                    }
                } // CLICK NOT ON THE STATION
                else {
                    workspace.getAddStationToLine().setSelected(false);
                    scene.setCursor(Cursor.DEFAULT);
                }
            } // SELECT A NODE
            else {
                // SELECT A LINE LABEL
                if (node instanceof DraggableText && workspace.getMetroLines().getItems().indexOf(((DraggableText) node).getText()) != -1) {
                    dataManager.highlightNode(node);
                    dataManager.setState(MMMState.MOVING_LINE_END);
                    // FIND THE LINE
                    for (MetroLine ml : dataManager.getLines()) {
                        if (ml.getId().equals(((DraggableText) node).getText())) {
                            metroLineToMove = ml;
//                            double disFromStart = Math.sqrt(Math.pow(((DraggableText) node).getX() - ml.getLineSegments().get(0).getStartX(), 2)
//                                    + Math.pow(((DraggableText) node).getY() - ml.getLineSegments().get(0).getStartY(), 2));
//                            double disFromEnd = Math.sqrt(Math.pow(((DraggableText) node).getX() - ml.getLineSegments().get(ml.getLineSegments().size() - 1).getEndX(), 2)
//                                    + Math.pow(((DraggableText) node).getY() - ml.getLineSegments().get(ml.getLineSegments().size() - 1).getEndY(), 2));
//                            if (disFromStart < disFromEnd) {
                            if (((DraggableText) node).getTextAlignment().equals(TextAlignment.RIGHT)) {
                                lineToMove = ml.getLineSegments().get(0);
                                oldX = lineToMove.getStartX();
                                oldY = lineToMove.getStartY();
                            } else {
                                lineToMove = ml.getLineSegments().get(ml.getLineSegments().size() - 1);
                                oldX = lineToMove.getEndX();
                                oldY = lineToMove.getEndY();
                            }
                            break;
                        }
                    }
                    startX = x;
                    startY = y;
                } // SELECT A STATION
                else if (node instanceof DraggableEllipse) {
                    dataManager.setState(MMMState.MOVING_STATION);
                    oldX = ((DraggableEllipse) node).getCenterX();
                    oldY = ((DraggableEllipse) node).getCenterY();
                } // SELECT A LINE
                else if (node instanceof Line) {
                    for (Node line : dataManager.getNodes()) {
                        if (!(line instanceof Line)) {
                            break;
                        }
                        if (line.getId().equals(node.getId())) {
                            dataManager.highlightNode(line);
                        }
                    }
                } // SELECT EITHER A IMAGE OR A TEXT
                else {

                }
                if (!(node instanceof DraggableImage)) {
                    workspace.loadSelectedNodeSettings(node);
                }
            }
        }

        workspace.reloadWorkspace(dataManager);
    }

    /**
     * Respond to mouse double presses on the rendering surface, which we call
     * canvas, but is actually a Pane.
     */
    public void processCanvasMouseDoublePress(int x, int y) {
        MMMWorkspace workspace = (MMMWorkspace) app.getWorkspaceComponent();
        MMMData data = (MMMData) app.getDataComponent();
        Node node = data.selectTopNode(x, y);
        Button ok = new Button("OK");
        Button close = new Button("CLOSE");
        Stage dialogStage = new Stage();
        // SELECTED NODE IS A LABEL
        if (node instanceof DraggableText && workspace.getMetroLines().getItems().indexOf(((DraggableText) node).getText()) == -1) {

        }
    }

    /**
     * Respond to mouse dragging on the rendering surface, which we call canvas,
     * but is actually a Pane.
     */
    public void processCanvasMouseDragged(int x, int y) {
        MMMData data = (MMMData) app.getDataComponent();
        if (!(data.getSelectedNode() instanceof Line)) {
            Draggable selectedDraggableShape = (Draggable) data.getSelectedNode();
            if (data.isInState(MMMState.MOVING_LINE_END)) {
                //selectedDraggableShape.drag(x, y);
//            double disFromStart = Math.sqrt(Math.pow(selectedDraggableShape.getX() - lineToMove.getStartX(), 2)
//                    + Math.pow(selectedDraggableShape.getY() - lineToMove.getStartY(), 2));
//            double disFromEnd = Math.sqrt(Math.pow(selectedDraggableShape.getX() - lineToMove.getEndX(), 2)
//                    + Math.pow(selectedDraggableShape.getY() - lineToMove.getEndY(), 2));
                if (((DraggableText) selectedDraggableShape).getTextAlignment().equals(TextAlignment.RIGHT)) {
                    double diffX = x - startX;
                    double diffY = y - startY;
                    double newX = lineToMove.getStartX() + diffX;
                    double newY = lineToMove.getStartY() + diffY;
                    lineToMove.setStartX(newX);
                    lineToMove.setStartY(newY);
                    startX = x;
                    startY = y;
                } else {
                    double diffX = x - startX;
                    double diffY = y - startY;
                    double newX = lineToMove.getEndX() + diffX;
                    double newY = lineToMove.getEndY() + diffY;
                    lineToMove.setEndX(newX);
                    lineToMove.setEndY(newY);
                    startX = x;
                    startY = y;
                }
            } else if (data.isInState(MMMState.MOVING_STATION)) {
                if (selectedDraggableShape != null) {
                    //dragNode = (Node) selectedDraggableShape;
                    selectedDraggableShape.drag(x, y);
                }
            }
        }
    }

    /**
     * Respond to mouse button release on the rendering surface, which we call
     * canvas, but is actually a Pane.
     */
    public void processCanvasMouseRelease(int x, int y) {
        MMMData data = (MMMData) app.getDataComponent();
        if (data.isInState(MMMState.MOVING_LINE_END)) {
            //MoveLineEndTran transaction = new MoveLineEndTran(data);
            // jTPS tps = app.getJTPS();
            //tps.addTransaction(transaction);
            data.setState(MMMState.DOING_NOTHING);
        } else if (data.isInState(MMMState.MOVING_STATION)) {
            data.setState(MMMState.DOING_NOTHING);
        }
    }
}
