package MMM.gui;

import MMM.data.DraggableEllipse;
import MMM.data.DraggableImage;
import MMM.data.DraggableText;
import java.io.File;
import java.io.IOException;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javax.imageio.ImageIO;
import MMM.data.MMMData;
import MMM.data.MMMState;
import MMM.data.MetroLine;
import MMM.tran.AddLineTran;
import MMM.tran.AddStationTran;
import MMM.tran.ChangeBackgroundColorTran;
import MMM.tran.ChangeFillColorTran;
import MMM.tran.ChangeThicknessTran;
import MMM.tran.RemoveLineTran;
import MMM.tran.RemoveStationTran;
import djf.AppTemplate;
import djf.settings.AppStartupConstants;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoDialogSingleton;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import jtps.jTPS;

/**
 * This class responds to interactions with other UI logo editing controls.
 *
 * @author Doeun Kim
 * @version 1.0
 */
public class MMMEditController {

    AppTemplate app;
    MMMData dataManager;
    String selectedLine;
    String selectedStation;

    public MMMEditController(AppTemplate initApp) {
        app = initApp;
        dataManager = (MMMData) app.getDataComponent();
    }

    public void handleEditLine() {
        MMMWorkspace workspace = (MMMWorkspace) app.getWorkspaceComponent();
        MMMData data = (MMMData) app.getDataComponent();
        MetroLine line = null;
        // FIND THE LINE
        for (MetroLine ml : data.getLines()) {
            if (ml.getId().equals(workspace.getMetroLines().getValue().toString())) {
                line = ml;
            }
        }
        Button ok = new Button("OK");
        Button close = new Button("CLOSE");
        Stage dialogStage = new Stage();
        Label promptLineName = new Label("Line name: ");
        TextField typeLineName = new TextField(workspace.getMetroLines().getValue().toString());
        HBox lineName = new HBox(promptLineName, typeLineName);
        lineName.setPadding(new Insets(20));
        Label promptLineColor = new Label("Line color: ");
        ColorPicker pickLineColor = new ColorPicker((Color) line.getLineSegments().get(0).getStroke());
        HBox lineColor = new HBox(promptLineColor, pickLineColor);
        lineColor.setPadding(new Insets(20));
        Label promptLineCircular = new Label("Line is circular: ");
        CheckBox isLineCircular = new CheckBox();
        isLineCircular.setSelected(line.getIsCircular());
        HBox lineCircular = new HBox(promptLineCircular, isLineCircular);
        lineCircular.setPadding(new Insets(20));
        HBox buttons = new HBox(ok, close);
        buttons.setSpacing(20);
        buttons.setAlignment(Pos.CENTER);
        VBox pane = new VBox(lineName, lineColor, lineCircular, buttons);
        pane.setPadding(new Insets(50));
        Scene dialogScene = new Scene(pane);
        dialogStage.setScene(dialogScene);
        dialogStage.setTitle("Edit Line");
        dialogStage.show();

        // ADD LISTENER TO OK BUTTON
        ok.setOnAction(e -> {
            //EditLineTran transaction = new EditLineTran(app, line);
            //jTPS tps = app.getJTPS();
            //tps.addTransaction(transaction);
            // app.getGUI().updateToolbarControls(false);
            // workspace.reloadWorkspace(data);
            
            MetroLine selectedLine = null;
            // FIND THE LINE
            for (MetroLine ml : data.getLines()) {
                if (ml.getId().equals(workspace.getMetroLines().getValue().toString())) {
                    selectedLine = ml;
                }
            }
            // LINE COLOR CHANGED
            for(Line l : selectedLine.getLineSegments()){
                l.setStroke(pickLineColor.getValue());
                l.setId(typeLineName.getText());
            }
            // LINE NAME CHANGED
            // UPDATE COMBOBOX
            int indexOfLine = workspace.getMetroLines().getItems().indexOf(selectedLine.getId());
            workspace.getMetroLines().getItems().remove(indexOfLine);
            workspace.getMetroLines().getItems().add(indexOfLine, typeLineName.getText());
            workspace.getMetroLines().setValue(typeLineName.getText());
            // UPDATE THE LINE LABEL TEXTS
            for (Node n : data.getNodes()) {
                if (n instanceof DraggableText && ((DraggableText) n).getText().equals(selectedLine.getId())) {
                    ((DraggableText) n).setText(typeLineName.getText());
                }
            }
            // CHANGE THE METRO LINE'S ID
            selectedLine.setId(typeLineName.getText());
            // CHANGE ISCIRCULAR
            selectedLine.setIsCircular(isLineCircular.isSelected());
            dialogStage.close();
             app.getGUI().updateToolbarControls(false);
                workspace.reloadWorkspace(dataManager);
        });
        // ADD LISTENER TO CLOSE BUTTON
        close.setOnAction(e -> {
            dialogStage.close();
        });
    }

    public void handleAddLine() {
        MMMWorkspace workspace = (MMMWorkspace) app.getWorkspaceComponent();
        //  CONSTRUCT A DIALOG
        Label promptLineName = new Label("Line name: ");
        TextField typeLineName = new TextField();
        HBox lineName = new HBox(promptLineName, typeLineName);
        lineName.setPadding(new Insets(20));
        Label promptLineColor = new Label("Line color: ");
        ColorPicker pickLineColor = new ColorPicker();
        HBox lineColor = new HBox(promptLineColor, pickLineColor);
        lineColor.setPadding(new Insets(20));
        Label promptLineCircular = new Label("Line is circular: ");
        CheckBox isLineCircular = new CheckBox();
        HBox lineCircular = new HBox(promptLineCircular, isLineCircular);
        lineCircular.setPadding(new Insets(20));
        Button ok = new Button("OK");
        Button close = new Button("CLOSE");
        HBox buttons = new HBox(ok, close);
        buttons.setAlignment(Pos.CENTER);
        buttons.setSpacing(20);
        VBox pane = new VBox(lineName, lineColor, lineCircular, buttons);
        pane.setPadding(new Insets(50));
        Scene dialogScene = new Scene(pane);
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Add New Line");
        dialogStage.setScene(dialogScene);

        // WHEN THE USER PRESS OK BUTTON TO ADD A LINE
        ok.setOnAction(eh -> {
            if (!typeLineName.getText().equals("") && pickLineColor.getValue() != null) {
                // LOAD COMBO BOX AND COLOR PICKER
                workspace.getMetroLines().getItems().add(typeLineName.getText());
                workspace.getMetroLines().setValue(typeLineName.getText());
                // CONSTRUCT A METRO LINE
                MetroLine line = new MetroLine(typeLineName.getText());
                // CONSTRUCT A REAL LINE SEGMENT
                Line lineSeg = new Line(100, 100, 200, 100);
                line.setIsCircular(isLineCircular.isSelected());
                lineSeg.setId(typeLineName.getText());
                lineSeg.setOpacity(1.0);
                lineSeg.setStroke((Color)pickLineColor.getValue());
                lineSeg.setStrokeWidth(workspace.getLineThickness().getValue());
                line.addLineSegment(lineSeg, dataManager);
                AddLineTran transaction = new AddLineTran((MMMData) app.getDataComponent(), line);
                jTPS tps = app.getJTPS();
                tps.addTransaction(transaction);
                dialogStage.close();
                //((MMMData) app.getDataComponent()).setSelectedNode(line);
                //((MMMData) app.getDataComponent()).highlightNode(line);
                app.getGUI().updateToolbarControls(false);
                workspace.reloadWorkspace(dataManager);
            } else {
                AppMessageDialogSingleton message = AppMessageDialogSingleton.getSingleton();
                message.show("Incompletion", "You haven't incompleted setting line properties");
            }
        });
        close.setOnAction(eh -> {
            dialogStage.close();
        });
        dialogStage.show();
    }

    public void handleRemoveLine() {
        MMMWorkspace workspace = (MMMWorkspace) app.getWorkspaceComponent();
        String lineName = workspace.getMetroLines().getValue().toString();
        AppYesNoDialogSingleton dialog = AppYesNoDialogSingleton.getSingleton();
        dialog.show("Remove Metro Line", "Do you really want to remove this line?");
        String selection = dialog.getSelection();
        if (selection.equals(AppYesNoDialogSingleton.YES)) {
            dialog.close();
            // FIND LINE SEGMENTS
            ArrayList<Line> lineSegs = new ArrayList<>();
            for (Node node : dataManager.getNodes()) {
                // FIND LINE SEGMENT
                if (node instanceof Line && !(node instanceof MetroLine) && ((Line) node).getId().equals(lineName)) {
                    lineSegs.add((Line) node);
                }
            }
            // REMOVE LINE FROM COMBOBOX
            workspace.getMetroLines().getItems().remove(lineName);
            // LOAD ANOTHER LINE NAME TO COMBOBOX
            if (workspace.getMetroLines().getItems().size() > 0) {
                workspace.getMetroLines().setValue(workspace.getMetroLines().getItems().get(0));
            }
            // REMOVE TRANSACTION
            RemoveLineTran transaction = new RemoveLineTran((MMMData) app.getDataComponent(), lineSegs);
            jTPS tps = app.getJTPS();
            tps.addTransaction(transaction);
            app.getGUI().updateToolbarControls(false);
            workspace.reloadWorkspace(dataManager);
        } else {
            dialog.close();
        }
    }

    public void handleAddStationToLine() {
        MMMWorkspace workspace = (MMMWorkspace) app.getWorkspaceComponent();
        ((MMMData) app.getDataComponent()).setState(MMMState.ADDING_STATION);
        Scene scene = app.getGUI().getPrimaryScene();
        // CHANGE THE CURSOR
        scene.setCursor(Cursor.HAND);
        workspace.reloadWorkspace(dataManager);
    }

    public void handleRemoveStationFromLine() {
        MMMWorkspace workspace = (MMMWorkspace) app.getWorkspaceComponent();
        ((MMMData) app.getDataComponent()).setState(MMMState.REMOVING_STATION);
        Scene scene = app.getGUI().getPrimaryScene();
        // CHANGE THE CURSOR
        scene.setCursor(Cursor.HAND);
        workspace.reloadWorkspace(dataManager);
    }

    public void handleShowList() {
        MMMWorkspace workspace = (MMMWorkspace) app.getWorkspaceComponent();
        String lineName = workspace.getMetroLines().getSelectionModel().getSelectedItem().toString();
        //CONSTRUCT DIALOG
        Label title = new Label();
        title.setFont(Font.font(lineName + " Line Stops", FontWeight.BOLD, 15));
        VBox list = new VBox();
        list.setAlignment(Pos.TOP_LEFT);
        list.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        Button ok = new Button("OK");
        VBox dialogPane = new VBox();
        dialogPane.getChildren().addAll(title, list, ok);
        Scene dialogScene = new Scene(dialogPane);
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Metro Map Maker - Metro Line Stops");
        dialogStage.setScene(dialogScene);
        ArrayList<DraggableEllipse> stations = null;
        for (MetroLine line : dataManager.getLines()) {
            if (line.getId().equals(((MMMWorkspace) app.getWorkspaceComponent()).getMetroLines().getSelectionModel().getSelectedItem())) {
                stations = line.getStations();
                break;
            }
        }
        for (DraggableEllipse station : stations) {
            Label label = new Label("● " + station.getId());
            label.setFont(Font.font(10));
            list.getChildren().add(label);
        }
        dialogStage.show();
    }

    public void handleChangeLineThickness() {
        MMMWorkspace workspace = (MMMWorkspace) app.getWorkspaceComponent();
        double lineThickness = workspace.getLineThickness().getValue();
        MMMData data = (MMMData) app.getDataComponent();
        Node node = data.getSelectedNode();
        if ((node != null) && node instanceof MetroLine) {
            ChangeThicknessTran transaction = new ChangeThicknessTran((Shape) node, lineThickness);
            jTPS tps = app.getJTPS();
            tps.addTransaction(transaction);
             app.getGUI().updateToolbarControls(false);
                workspace.reloadWorkspace(dataManager);
        }
    }

    public void handleMetroStationColorPicker() {
        MMMWorkspace workspace = (MMMWorkspace) app.getWorkspaceComponent();
        Color selectedColor = workspace.getMetroStationColorPicker().getValue();
        if (selectedColor != null) {
            MMMData data = (MMMData) app.getDataComponent();
            Node node = data.getSelectedNode();
            if (node != null && node instanceof DraggableEllipse) {
                ChangeFillColorTran transaction = new ChangeFillColorTran((Shape) node, selectedColor);
                jTPS tps = app.getJTPS();
                tps.addTransaction(transaction);
                app.getGUI().updateToolbarControls(false);
                workspace.reloadWorkspace(dataManager);
            }
        }
    }

    public void handleAddStation() {
        // DraggableEllipse station = new DraggableEllipse();
        MMMWorkspace workspace = (MMMWorkspace) app.getWorkspaceComponent();
        MMMData data = (MMMData) app.getDataComponent();
        // CONSTRUCT THE DIALOG
        Label promptStationName = new Label("Station name: ");
        TextField typeStationName = new TextField();
        HBox lineName = new HBox(promptStationName, typeStationName);
        lineName.setPadding(new Insets(20));
        Button ok = new Button("OK");
        Button close = new Button("CLOSE");
        HBox buttons = new HBox(ok, close);
        buttons.setPadding(new Insets(20));
        VBox pane = new VBox(lineName, buttons);
        pane.setPadding(new Insets(50));
        Scene dialogScene = new Scene(pane);
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Add New Station");
        dialogStage.setScene(dialogScene);
        dialogStage.show();
        // ADD LISTENER TO OK BUTTON
        ok.setOnAction(e -> {
            if (!typeStationName.getText().equals("")) {
                // LOAD COMBO BOX 
                workspace.getMetroStations().getItems().add(typeStationName.getText());
                workspace.getMetroStations().setValue(typeStationName.getText());
                // ADD NEW LINE
                DraggableEllipse station = new DraggableEllipse(typeStationName.getText());
                AddStationTran transaction = new AddStationTran(data, station);
                jTPS tps = app.getJTPS();
                tps.addTransaction(transaction);
                dialogStage.close();
                //data.setSelectedNode(station);
                //data.highlightNode(station);
                app.getGUI().updateToolbarControls(false);
                workspace.reloadWorkspace(dataManager);
            } else {
                // ERROR
                AppMessageDialogSingleton error = AppMessageDialogSingleton.getSingleton();
                error.show("Incompletion", "You haven't incompleted setting a station property");
            }
        });
        // ADD LISTENER TO CLOSE BUTTON
        close.setOnAction(e -> {
            dialogStage.close();
        });
    }

    public void handleRemoveStation() {
        MMMWorkspace workspace = (MMMWorkspace) app.getWorkspaceComponent();
        MMMData data = (MMMData) app.getDataComponent();
        String stationName = workspace.getMetroStations().getValue().toString();
        AppYesNoDialogSingleton dialog = AppYesNoDialogSingleton.getSingleton();
        dialog.show("Remove Metro Station", "Do you really want to remove this station?");
        String selection = dialog.getSelection();
        if (selection.equals(AppYesNoDialogSingleton.YES)) {
            dialog.close();
            // REMOVE FROM COMBOBOX
            workspace.getMetroStations().getItems().remove(stationName);
            // LOAD ANOTHER STATION NAME TO COMBOBOX
            if (workspace.getMetroStations().getItems().size() > 0) {
                workspace.getMetroStations().setValue(workspace.getMetroStations().getItems().get(0));
            }
            // UPDATE COLOR PICKER
            workspace.getMetroStationColorPicker().setValue(Color.BLACK);
            DraggableText stationLabelToRemove = null;
            DraggableEllipse stationToRemove = null;
            for (Node n : data.getNodes()) {
                // FIND THE STATION LABEL
                if (n instanceof DraggableText && ((DraggableText) n).getText().equals(stationName)) {
                    stationLabelToRemove = (DraggableText) n;
                } // FIND THE CORRESPONDING STATION
                else if (n instanceof DraggableEllipse && n.getId().equals(stationName)) {
                    stationToRemove = (DraggableEllipse) n;
                }
            }
            RemoveStationTran transaction = new RemoveStationTran((MMMData) app.getDataComponent(), stationToRemove, stationLabelToRemove);
            jTPS tps = app.getJTPS();
            tps.addTransaction(transaction);
            app.getGUI().updateToolbarControls(false);
            workspace.reloadWorkspace(dataManager);
        } else {
            dialog.close();
        }
    }

    public void handleSnap() {

    }

    public void handleMoveLabel() {

    }

    public void handleRotateLabel() {

    }

    public void handleChangeRadius() {

    }

    public void handleFindShortestPath() {
        MMMWorkspace workspace = (MMMWorkspace) app.getWorkspaceComponent();
        //CONSTRUCT DIALOG
        String departure = workspace.getDeparture().getSelectionModel().getSelectedItem().toString();
        String arrival = workspace.getDeparture().getSelectionModel().getSelectedItem().toString();
        Label title = new Label();
        title.setFont(Font.font("Route from " + departure + " to " + arrival, FontWeight.BOLD, 15));
        VBox main = new VBox();
        main.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        Button ok = new Button("OK");
        VBox dialogPane = new VBox();
        dialogPane.getChildren().addAll(title, main, ok);
        Scene dialogScene = new Scene(dialogPane);
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Metro Map Maker - Route");
        dialogStage.setScene(dialogScene);
        //CONSTRUCT ROUTE
        boolean haveToTransfer = false;

        dialogStage.show();
    }

    public void handleBackgroundColorPicker() {
        MMMWorkspace workspace = (MMMWorkspace) app.getWorkspaceComponent();
        Color selectedColor = workspace.getBackgroundColorPicker().getValue();
        if (selectedColor != null) {
            Pane grid = workspace.getGrid();
            ChangeBackgroundColorTran transaction = new ChangeBackgroundColorTran(grid, selectedColor);
            jTPS tps = app.getJTPS();
            tps.addTransaction(transaction);
            app.getGUI().updateToolbarControls(false);
            workspace.reloadWorkspace(dataManager);
        }
    }

    public void handleSetImageBackground() {

    }

    public void handleAddImage() {
        MMMWorkspace workspace = (MMMWorkspace) app.getWorkspaceComponent();
        // ASK THE USER TO SELECT AN IMAGE
        Image imageToAdd = promptForImage();
        if (imageToAdd != null) {
            DraggableImage imageViewToAdd = new DraggableImage();
            imageViewToAdd.setImage(imageToAdd);
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            imageViewToAdd.xProperty().set(100);
            imageViewToAdd.yProperty().set(100);

            // MAKE AND ADD THE TRANSACTION
            jTPS tps = app.getJTPS();
            //tps.addTransaction(transaction);
            app.getGUI().updateToolbarControls(false);
            workspace.reloadWorkspace(dataManager);
            //addNodeTransaction(imageViewToAdd);
        }
    }

    private Image promptForImage() {
        // SETUP THE FILE CHOOSER FOR PICKING IMAGES
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File("./images/"));
        //FileChooser.ExtensionFilter extFilterBMP = new FileChooser.ExtensionFilter("BMP files (*.bmp)", "*.BMP");
        //FileChooser.ExtensionFilter extFilterGIF = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
        //FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
        FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
        //fileChooser.getExtensionFilters().addAll(extFilterBMP, extFilterGIF, extFilterJPG, extFilterPNG);
        fileChooser.getExtensionFilters().addAll(extFilterPNG);
        fileChooser.setSelectedExtensionFilter(extFilterPNG);

        // OPEN THE DIALOG
        File file = fileChooser.showOpenDialog(null);
        try {
            BufferedImage bufferedImage = ImageIO.read(file);
            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
            return image;
        } catch (IOException ex) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("Error in Selecting Image", "Error in selecting image");
            return null;
        }
    }

    public void handleAddLabel() {

    }

    public void handleRemoveElement() {

    }

    public void handleFontColorPicker() {

    }

    public void handleSetBold() {

    }

    public void handleSetItalic() {

    }

    public void handleChangeFontSize() {

    }

    public void handleChangeFontFamily() {

    }

    public void handleShowGrid() {

    }

    public void handleZoomIn() {

    }

    public void handleZoomOut() {

    }

    public void handleIncreaseMapSize() {

    }

    public void handleDecreaseMapSize() {

    }

    public void handleExport() {
        processSnapshot();
    }

    public void handleUndo() {
        MMMWorkspace workspace = (MMMWorkspace) app.getWorkspaceComponent();
        app.getJTPS().undoTransaction();
        workspace.reloadWorkspace(dataManager);
    }

    public void handleRedo() {
        MMMWorkspace workspace = (MMMWorkspace) app.getWorkspaceComponent();
        app.getJTPS().doTransaction();
        workspace.reloadWorkspace(dataManager);
    }

    public void handleLearnAbout() {
        Stage aboutStage = new Stage();
        aboutStage.setTitle("About the application");
        VBox pane = new VBox();
        Label name = new Label("App Name: Metro Map Maker");
        Label framework = new Label("Framework: DesktopJavaFramekwork, PropertiesManager, jTPS");
        Label developer = new Label("Developer: Doeun Kim");
        Label year = new Label("2017");
        pane.getChildren().addAll(name, framework, developer, year);
        pane.setPadding(new Insets(80, 60, 80, 60));
        pane.setAlignment(Pos.CENTER);
        pane.setSpacing(20);
        Scene aboutScene = new Scene(pane);
        aboutStage.setScene(aboutScene);
        aboutStage.show();
    }
//    public void processSelectSelectionTool() {
//	// CHANGE THE CURSOR
//	Scene scene = app.getGUI().getPrimaryScene();
//	scene.setCursor(Cursor.DEFAULT);
//	
//	// CHANGE THE STATE
//	dataManager.setState(MMMState.SELECTING_SHAPE);	
//	
//	// ENABLE/DISABLE THE PROPER BUTTONS
//	MMMWorkspace workspace = (MMMWorkspace)app.getWorkspaceComponent();
//	workspace.reloadWorkspace(dataManager);
//    }
//    
//    /**
//     * This method handles a user request to remove the selected shape.
//     */
//    public void processRemoveSelectedShape() {
//	// REMOVE THE SELECTED SHAPE IF THERE IS ONE
//	dataManager.removeSelectedShape();
//	
//	// ENABLE/DISABLE THE PROPER BUTTONS
//	MMMWorkspace workspace = (MMMWorkspace)app.getWorkspaceComponent();
//	workspace.reloadWorkspace(dataManager);
//	app.getGUI().updateToolbarControls(false);
//    }
//    
//    /**
//     * This method processes a user request to start drawing a rectangle.
//     */
//    public void processSelectRectangleToDraw() {
//	// CHANGE THE CURSOR
//	Scene scene = app.getGUI().getPrimaryScene();
//	scene.setCursor(Cursor.CROSSHAIR);
//	
//	// CHANGE THE STATE
//	dataManager.setState(MMMState.STARTING_RECTANGLE);
//
//	// ENABLE/DISABLE THE PROPER BUTTONS
//	MMMWorkspace workspace = (MMMWorkspace)app.getWorkspaceComponent();
//	workspace.reloadWorkspace(dataManager);
//    }
//    
//    /**
//     * This method provides a response to the user requesting to start
//     * drawing an ellipse.
//     */
//    public void processSelectEllipseToDraw() {
//	// CHANGE THE CURSOR
//	Scene scene = app.getGUI().getPrimaryScene();
//	scene.setCursor(Cursor.CROSSHAIR);
//	
//	// CHANGE THE STATE
//	dataManager.setState(MMMState.STARTING_ELLIPSE);
//
//	// ENABLE/DISABLE THE PROPER BUTTONS
//	MMMWorkspace workspace = (MMMWorkspace)app.getWorkspaceComponent();
//	workspace.reloadWorkspace(dataManager);
//    }
//    
//    /**
//     * This method processes a user request to move the selected shape
//     * down to the back layer.
//     */
//    public void processMoveSelectedShapeToBack() {
//	dataManager.moveSelectedShapeToBack();
//	app.getGUI().updateToolbarControls(false);
//    }
//    
//    /**
//     * This method processes a user request to move the selected shape
//     * up to the front layer.
//     */
//    public void processMoveSelectedShapeToFront() {
//	dataManager.moveSelectedShapeToFront();
//	app.getGUI().updateToolbarControls(false);
//    }
//        
//
//    
//    /**
//     * This method processes a user request to select the outline
//     * thickness for shape drawing.
//     */
//    public void processSelectOutlineThickness() {
//	MMMWorkspace workspace = (MMMWorkspace)app.getWorkspaceComponent();
//	int outlineThickness = (int)workspace.getOutlineThicknessSlider().getValue();
//	dataManager.setCurrentOutlineThickness(outlineThickness);
//	app.getGUI().updateToolbarControls(false);
//    }
//    

    /**
     * This method processes a user request to take a snapshot of the current
     * scene.
     */
    public void processSnapshot() {
        MMMWorkspace workspace = (MMMWorkspace) app.getWorkspaceComponent();
        Pane canvas = workspace.getElement();
        WritableImage image = canvas.snapshot(new SnapshotParameters(), null);
        String fileName = app.getGUI().getWindow().getTitle().replace("Metro Map Maker - ", "");
        File file = new File(AppStartupConstants.PATH_EXPORT + "/" + fileName + "/" + fileName + " Metro.png");
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
