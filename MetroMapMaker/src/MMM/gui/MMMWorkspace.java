package MMM.gui;

import MMM.MMMLanguageProperty;
import java.io.IOException;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import MMM.data.MMMData;
import djf.ui.AppYesNoCancelDialogSingleton;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppGUI;
import djf.AppTemplate;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import static MMM.css.MMMStyle.*;
import MMM.data.DraggableEllipse;
import MMM.data.DraggableText;
import djf.settings.AppPropertyType;
import djf.settings.AppStartupConstants;
import java.io.File;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Line;
import properties_manager.PropertiesManager;

/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Doeun Kim
 * @version 1.0
 */
public class MMMWorkspace extends AppWorkspaceComponent {

    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;

    // THIS IS WHERE WE'LL RENDER OUR DRAWING, NOTE THAT WE
    // CALL THIS A CANVAS, BUT IT'S REALLY JUST A Pane
    ScrollPane scroll;
    Pane canvas;
    Pane element;
    Pane grid;
    // HERE ARE THE CONTROLLERS
    MMMCanvasController canvasController;
    MMMEditController editController;
    MMMKeyController keyController;

    // HAS ALL THE CONTROLS FOR EDITING
    VBox editToolbar;

    // FIRST ROW
    VBox row1;
    HBox row11;
    Label metroLinesText;
    ComboBox metroLines;
    Button editLine;
    HBox row12;
    Button addLine;
    Button removeLine;
    ToggleButton addStationToLine;
    ToggleButton removeStationFromLine;
    Button list;
    HBox row13;
    Slider lineThickness;

    // SECOND ROW
    VBox row2;
    HBox row21;
    Label metroStationsText;
    ComboBox metroStations;
    ColorPicker metroStationColor;
    HBox row22;
    Button addStation;
    Button removeStation;
    Button snap;
    Button moveLabel;
    Button rotateLabel;
    HBox row23;
    Slider radius;

    // THIRD ROW
    HBox row3;
    VBox row31;
    ComboBox departure;
    ComboBox arrival;
    VBox row32;
    Button route;

    // FORTH ROW
    VBox row4;
    HBox row41;
    Label decor;
    ColorPicker backgroundColor;
    HBox row42;
    Button setImageBackground;
    Button addImage;
    Button addLabel;
    Button removeElement;

    // FIFTH ROW
    VBox row5;
    HBox row51;
    Label font;
    ColorPicker fontColor;
    HBox row52;
    Button bold;
    Button italic;
    ComboBox size;
    ComboBox family;

    // SIXTH ROW
    VBox row6;
    HBox row61;
    Label navigation;
    CheckBox showGrid;
    Label showGridText;
    HBox row62;
    Button zoomIn;
    Button zoomOut;
    Button increaseMapSize;
    Button decreaseMapSize;

    Button saveAs;
    Button export;
    Button undo;
    Button redo;
    Button about;

    // HERE ARE OUR DIALOGS
    AppMessageDialogSingleton messageDialog;
    AppYesNoCancelDialogSingleton yesNoCancelDialog;
    MMMData data;

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public MMMWorkspace(AppTemplate initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // KEEP THE GUI FOR LATER
        gui = app.getGUI();

        // LAYOUT THE APP
        initLayout();

        // HOOK UP THE CONTROLLERS
        initControllers();

        // AND INIT THE STYLE FOR THE WORKSPACE
        initStyle();

    }
//    
//    // ACCESSOR METHODS FOR COMPONENTS THAT EVENT HANDLERS
//    // MAY NEED TO UPDATE OR ACCESS DATA FROM
//    
//    public ColorPicker getFillColorPicker() {
//	return fillColorPicker;
//    }
//    
//    public ColorPicker getOutlineColorPicker() {
//	return outlineColorPicker;
//    }
//    

    public ColorPicker getBackgroundColorPicker() {
        return backgroundColor;
    }
//    
//    public Slider getOutlineThicknessSlider() {
//	return outlineThicknessSlider;
//    }

    public ToggleButton getAddStationToLine() {
        return addStationToLine;
    }

    public ToggleButton getRemoveStationFromLine() {
        return removeStationFromLine;
    }

    public ComboBox getDeparture() {
        return departure;
    }

    public ComboBox getArrival() {
        return arrival;
    }

    public ComboBox getMetroLines() {
        return metroLines;
    }

    public ComboBox getMetroStations() {
        return metroStations;
    }

    public Pane getGrid() {
        return grid;
    }

    public Pane getElement() {
        return element;
    }

    public Pane getCanvas() {
        return canvas;
    }

    public ColorPicker getMetroStationColorPicker() {
        return metroStationColor;
    }

    public Slider getLineThickness() {
        return lineThickness;
    }

    // HELPER SETUP METHOD
    private void initLayout() {
        // THIS WILL GO IN THE LEFT SIDE OF THE WORKSPACE
        editToolbar = new VBox();

        // ROW 1
        row1 = new VBox();
        row11 = new HBox();
        row11.setSpacing(10);
        metroLinesText = new Label("Metro Lines");
        metroLines = new ComboBox();
        metroLines.setPromptText("Metro Line");
        editLine = new Button("Edit Line");
        row11.getChildren().addAll(metroLinesText, metroLines, editLine);
        row12 = new HBox();
        row12.setSpacing(10);
        addLine = new Button("+");
        removeLine = new Button("-");
        addStationToLine = new ToggleButton("Add Station");
        removeStationFromLine = new ToggleButton("Remove Station");
        row12.getChildren().addAll(addLine, removeLine, addStationToLine, removeStationFromLine);
        list = gui.initChildButton(row12, MMMLanguageProperty.LIST_ICON.toString(), MMMLanguageProperty.LIST_TOOLTIP.toString(), false);
        lineThickness = new Slider(1, 10, 3);
        row1.getChildren().addAll(row11, row12, lineThickness);

        // ROW 2
        row2 = new VBox();
        row21 = new HBox();
        row21.setSpacing(10);
        metroStationsText = new Label("Metro Stations");
        metroStations = new ComboBox();
        metroStations.setPromptText("Metro Station");
        metroStationColor = new ColorPicker(Color.BLACK);
        row21.getChildren().addAll(metroStationsText, metroStations, metroStationColor);
        row22 = new HBox();
        row22.setSpacing(10);
        addStation = new Button("+");
        removeStation = new Button("-");
        snap = new Button("Snap");
        moveLabel = new Button("Move Label");
        row22.getChildren().addAll(addStation, removeStation, snap, moveLabel);
        rotateLabel = gui.initChildButton(row22, MMMLanguageProperty.ROTATE_ICON.toString(), MMMLanguageProperty.ROTATE_TOOLTIP.toString(), false);
        radius = new Slider(10, 20, 10);
        row2.getChildren().addAll(row21, row22, radius);

        // ROW 3
        row3 = new HBox();
        row31 = new VBox();
        row31.setSpacing(10);
        departure = new ComboBox();
        departure.setPromptText("Departure");
        departure.setPrefWidth(200);
        arrival = new ComboBox();
        arrival.setPromptText("Arrival");
        arrival.setPrefWidth(200);
        row31.getChildren().addAll(departure, arrival);
        row32 = new VBox();
        row32.setSpacing(10);
        route = gui.initChildButton(row32, MMMLanguageProperty.ROUTE_ICON.toString(), MMMLanguageProperty.ROUTE_TOOLTIP.toString(), false);
        row3.getChildren().addAll(row31, row32);

        // ROW 4
        row4 = new VBox();
        row41 = new HBox();
        row41.setSpacing(10);
        decor = new Label("Decor");
        backgroundColor = new ColorPicker(Color.WHITE);
        row41.getChildren().addAll(decor, backgroundColor);
        row42 = new HBox();
        row42.setSpacing(10);
        setImageBackground = new Button("Set Image Background");
        addImage = new Button("Add Image");
        addLabel = new Button("Add Label");
        removeElement = new Button("Remove Element");
        row42.getChildren().addAll(setImageBackground, addImage, addLabel, removeElement);
        row4.getChildren().addAll(row41, row42);

        // ROW 5
        row5 = new VBox();
        row51 = new HBox();
        row51.setSpacing(10);
        font = new Label("Font");
        fontColor = new ColorPicker(Color.BLACK);
        row51.getChildren().addAll(font, fontColor);
        row52 = new HBox();
        row52.setSpacing(10);
        bold = gui.initChildButton(row52, MMMLanguageProperty.BOLD_ICON.toString(), MMMLanguageProperty.BOLD_TOOLTIP.toString(), false);
        italic = gui.initChildButton(row52, MMMLanguageProperty.ITALIC_ICON.toString(), MMMLanguageProperty.ITALIC_TOOLTIP.toString(), false);
        size = new ComboBox();
        size.setPromptText("Font Size");
        size.getItems().addAll("10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20");
        family = new ComboBox();
        family.setPromptText("Font Family");
        family.getItems().addAll("Times New Roman", "Arial", "Georgia", "Calibri", "Verdana");
        row52.getChildren().addAll(size, family);
        row5.getChildren().addAll(row51, row52);

        // ROW 6
        row6 = new VBox();
        row61 = new HBox();
        row61.setSpacing(10);
        navigation = new Label("Navigation");
        showGrid = new CheckBox("Show Grid");
        row61.getChildren().addAll(navigation, showGrid);
        row62 = new HBox();
        row62.setSpacing(10);
        zoomIn = gui.initChildButton(row62, MMMLanguageProperty.ZOOMIN_ICON.toString(), MMMLanguageProperty.ZOOMIN_TOOLTIP.toString(), false);
        zoomOut = gui.initChildButton(row62, MMMLanguageProperty.ZOOMOUT_ICON.toString(), MMMLanguageProperty.ZOOMOUT_TOOLTIP.toString(), false);
        decreaseMapSize = gui.initChildButton(row62, MMMLanguageProperty.DECREASE_ICON.toString(), MMMLanguageProperty.DECREASE_TOOLTIP.toString(), false);
        increaseMapSize = gui.initChildButton(row62, MMMLanguageProperty.INCREASE_ICON.toString(), MMMLanguageProperty.INCREASE_TOOLTIP.toString(), false);
        row6.getChildren().addAll(row61, row62);

        // NOW ORGANIZE THE EDIT TOOLBAR
        editToolbar.getChildren().add(row1);
        editToolbar.getChildren().add(row2);
        editToolbar.getChildren().add(row3);
        editToolbar.getChildren().add(row4);
        editToolbar.getChildren().add(row5);
        editToolbar.getChildren().add(row6);

        FlowPane fileToolbar = app.getGUI().getFileToolbar();
        saveAs = gui.initChildButton(fileToolbar, MMMLanguageProperty.SAVEAS_ICON.toString(), "", false);
        export = gui.initChildButton(fileToolbar, MMMLanguageProperty.EXPORT_ICON.toString(), "", false);
        undo = gui.initChildButton(fileToolbar, MMMLanguageProperty.UNDO_ICON.toString(), "", true);
        redo = gui.initChildButton(fileToolbar, MMMLanguageProperty.REDO_ICON.toString(), "", true);
        about = gui.initChildButton(fileToolbar, MMMLanguageProperty.ABOUT_ICON.toString(), "", false);
        // WE'LL RENDER OUR STUFF HERE IN THE CANVAS
        canvas = new StackPane();
        grid = new Pane();
        grid.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        element = new Pane();
        element.setStyle("-fx-background-color: rgba(255, 255, 255, 0);");
        canvas.getChildren().addAll(grid, element);
        scroll = new ScrollPane(canvas);

        // AND MAKE SURE THE DATA MANAGER IS IN SYNCH WITH THE PANE
        data = (MMMData) app.getDataComponent();
        data.setNodes(element.getChildren());

        // AND NOW SETUP THE WORKSPACE
        workspace = new BorderPane();
        ((BorderPane) workspace).setLeft(editToolbar);
        ((BorderPane) workspace).setCenter(canvas);

    }

    // HELPER SETUP METHOD
    private void initControllers() {
        // MAKE THE EDIT CONTROLLER
        editController = new MMMEditController(app);

        // NOW CONNECT THE BUTTONS TO THEIR HANDLERS
        editLine.setOnAction(e -> {
            editController.handleEditLine();
        });
        addLine.setOnAction(e -> {
            editController.handleAddLine();
        });
        removeLine.setOnAction(e -> {
            editController.handleRemoveLine();
        });
        addStationToLine.setOnAction(e -> {
            addStationToLine.setSelected(true);
            editController.handleAddStationToLine();
        });
        removeStationFromLine.setOnAction(e -> {
            editController.handleRemoveStationFromLine();
        });
        list.setOnAction(e -> {
            editController.handleShowList();
        });
        lineThickness.setOnMouseReleased(e -> {
            editController.handleChangeLineThickness();
        });
        metroStationColor.setOnAction(e -> {
            editController.handleMetroStationColorPicker();
        });
        addStation.setOnAction(e -> {
            editController.handleAddStation();
        });
        removeStation.setOnAction(e -> {
            editController.handleRemoveStation();
        });
        snap.setOnAction(e -> {
            editController.handleSnap();
        });
        moveLabel.setOnAction(e -> {
            editController.handleMoveLabel();
        });
        rotateLabel.setOnAction(e -> {
            editController.handleRotateLabel();
        });
        radius.setOnMouseReleased(e -> {
            editController.handleChangeRadius();
        });
        route.setOnAction(e -> {
            editController.handleFindShortestPath();
        });
        backgroundColor.setOnAction(e -> {
            editController.handleBackgroundColorPicker();
        });
        setImageBackground.setOnAction(e -> {
            editController.handleSetImageBackground();
        });
        addImage.setOnAction(e -> {
            editController.handleAddImage();
        });
        addLabel.setOnAction(e -> {
            editController.handleAddLabel();
        });
        removeElement.setOnAction(e -> {
            editController.handleRemoveElement();
        });
        fontColor.setOnAction(e -> {
            editController.handleFontColorPicker();
        });
        bold.setOnAction(e -> {
            editController.handleSetBold();
        });
        italic.setOnAction(e -> {
            editController.handleSetItalic();
        });
        size.setOnAction(e -> {
            editController.handleChangeFontSize();
        });
        family.setOnAction(e -> {
            editController.handleChangeFontFamily();
        });
        showGrid.setOnAction(e -> {
            editController.handleShowGrid();
        });
        zoomIn.setOnAction(e -> {
            editController.handleZoomIn();
        });
        zoomOut.setOnAction(e -> {
            editController.handleZoomOut();
        });
        increaseMapSize.setOnAction(e -> {
            editController.handleIncreaseMapSize();
        });
        decreaseMapSize.setOnAction(e -> {
            editController.handleDecreaseMapSize();
        });
        undo.setOnAction(e -> {
            editController.handleUndo();
        });
        redo.setOnAction(e -> {
            editController.handleRedo();
        });
        about.setOnAction(e -> {
            editController.handleLearnAbout();
        });
        saveAs.setOnAction(e -> {
            app.getGUI().getFileController().handleSaveAsRequest();
        });
        export.setOnAction(e -> {
            try {
                String fileName = app.getGUI().getWindow().getTitle().replace("Metro Map Maker - ", "");
                File newFile = new File(AppStartupConstants.PATH_EXPORT + "/" + fileName + "/" + fileName + " Metro.json");
                app.getFileComponent().exportData(data, newFile.getPath());
                editController.processSnapshot();
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show("Export Done", "Exporting the map image and the map data has been successfully completed.");
            } catch (Exception ex) {
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(AppPropertyType.EXPORT_ERROR_TITLE), props.getProperty(AppPropertyType.EXPORT_ERROR_MESSAGE));
            }
        });
        // MAKE THE CANVAS CONTROLLER	
        canvasController = new MMMCanvasController(app);
        canvas.setOnMousePressed(e -> {
            canvasController.processCanvasMousePress((int) e.getX(), (int) e.getY());
        });
        canvas.setOnMouseReleased(e -> {
            canvasController.processCanvasMouseRelease((int) e.getX(), (int) e.getY());
        });
        canvas.setOnMouseDragged(e -> {
            canvasController.processCanvasMouseDragged((int) e.getX(), (int) e.getY());
        });
        app.getGUI().getWindow().setOnCloseRequest(eh -> {
            app.getGUI().getFileController().handleExitRequest();
        });
    }

    public void loadSelectedNodeSettings(Node node) {
        if (node != null) {
            if (node instanceof Line) {
                metroLines.setValue(node.getId());
                lineThickness.setValue(((Line) node).getStrokeWidth());
            } else if (node instanceof DraggableText) {
                fontColor.setValue((Color) ((DraggableText) node).getFill());
                family.setValue(((DraggableText) node).getFont().getFamily());
            } else if (node instanceof DraggableEllipse) {
                metroStationColor.setValue((Color) ((DraggableEllipse) node).getFill());
                metroStations.setValue(node.getId());
                radius.setValue(((DraggableEllipse) node).getRadiusX());
            }
        }
    }

    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    public void initStyle() {
        // NOTE THAT EACH CLASS SHOULD CORRESPOND TO
        // A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
        // CSS FILE
        canvas.getStyleClass().add(CLASS_RENDER_CANVAS);

        // COLOR PICKER STYLE
        editLine.getStyleClass().add(CLASS_BUTTON);
        metroStationColor.getStyleClass().add(CLASS_BUTTON);
        backgroundColor.getStyleClass().add(CLASS_BUTTON);
        fontColor.getStyleClass().add(CLASS_BUTTON);

        editToolbar.getStyleClass().add(CLASS_EDIT_TOOLBAR);
        row1.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row2.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row3.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row4.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row5.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row6.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        metroLinesText.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
        metroStationsText.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
        decor.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
        font.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
        navigation.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
    }

    /**
     * This function reloads all the controls for editing logos the workspace.
     */
    @Override
    public void reloadWorkspace(AppDataComponent data) {
        MMMData dataManager = (MMMData) data;
        if (app.getJTPS().isTheMostRecentTransaction() && app.getJTPS().getMostRecentTransaction() != -1 && app.getJTPS().getTransactionsSize() > 0) {
            undo.setDisable(false);
            redo.setDisable(true);
        } else if (app.getJTPS().getTransactionsSize() == 0) {
            undo.setDisable(true);
            redo.setDisable(true);
        } else if (app.getJTPS().getMostRecentTransaction() == -1 && app.getJTPS().getTransactionsSize() > 0) {
            undo.setDisable(true);
            redo.setDisable(false);
        } else {
            undo.setDisable(false);
            redo.setDisable(false);
        }
//        if (dataManager.isInState(MMMState.STARTING_RECTANGLE)) {
//            selectionToolButton.setDisable(false);
//            removeButton.setDisable(true);
//            rectButton.setDisable(true);
//            ellipseButton.setDisable(false);
//        } else if (dataManager.isInState(MMMState.STARTING_ELLIPSE)) {
//            selectionToolButton.setDisable(false);
//            removeButton.setDisable(true);
//            rectButton.setDisable(false);
//            ellipseButton.setDisable(true);
//        } else if (dataManager.isInState(MMMState.SELECTING_SHAPE)
//                || dataManager.isInState(MMMState.DRAGGING_SHAPE)
//                || dataManager.isInState(MMMState.DRAGGING_NOTHING)) {
//            boolean shapeIsNotSelected = dataManager.getSelectedShape() == null;
//            selectionToolButton.setDisable(true);
//            removeButton.setDisable(shapeIsNotSelected);
//            rectButton.setDisable(false);
//            ellipseButton.setDisable(false);
//            moveToFrontButton.setDisable(shapeIsNotSelected);
//            moveToBackButton.setDisable(shapeIsNotSelected);
//        }
//
//        removeButton.setDisable(dataManager.getSelectedShape() == null);
//        backgroundColorPicker.setValue(dataManager.getBackgroundColor());
        app.getGUI().getFileController().markAsEdited(gui);
    }

    @Override
    public void resetWorkspace() {
        // WE ARE NOT USING THIS, THOUGH YOU MAY IF YOU LIKE
    }
}
