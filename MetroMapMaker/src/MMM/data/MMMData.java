package MMM.data;

import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Shape;
import MMM.gui.MMMWorkspace;
import djf.components.AppDataComponent;
import djf.AppTemplate;
import javafx.geometry.Insets;
import javafx.scene.image.ImageView;
import javafx.scene.layout.CornerRadii;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

/**
 * This class serves as the data management component for this application.
 *
 * @author Doeun Kim
 * @version 1.0
 */
public class MMMData implements AppDataComponent {

    // THESE ARE THE NODES IN THE LOGO
    ObservableList<Node> nodes;
    ArrayList<MetroLine> lines;
//HashMap<String, Line> lines;
    //HashMap<String, DraggableEllipse> stations;
    //HashMap<MetroLine, ArrayList<DraggableEllipse>> connections;
    // THIS IS THE SHAPE CURRENTLY BEING SIZED BUT NOT YET ADDED
    Shape newShape;

    // THIS IS THE NODE CURRENTLY SELECTED
    Node selectedNode;

    // CURRENT STATE OF THE APP
    MMMState state;

    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;

    // USE THIS WHEN THE NODE IS SELECTED
    Effect highlightedEffect;
    ImageView backgroundImage;

    public static final String WHITE_HEX = "#FFFFFF";
    public static final String BLACK_HEX = "#000000";
    public static final String YELLOW_HEX = "#EEEE00";
    public static final Paint DEFAULT_BACKGROUND_COLOR = Paint.valueOf(WHITE_HEX);
    public static final Paint HIGHLIGHTED_COLOR = Paint.valueOf(YELLOW_HEX);
    public static final int HIGHLIGHTED_STROKE_THICKNESS = 3;

    // THE BACKGROUND COLOR
    Color backgroundColor;
// FOR FILL AND OUTLINE
    Color currentFillColor;
    Color currentOutlineColor;
    double currentBorderWidth;
    //int metroLineIndex = 0;

    /**
     * THis constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public MMMData(AppTemplate initApp) {
        // KEEP THE APP FOR LATER
        app = initApp;

        // NO SHAPE STARTS OUT AS SELECTED
        newShape = null;
        selectedNode = null;
        lines = new ArrayList<>();
        //stations = new HashMap<>();
        //lines = new HashMap<>();
        //connections = new HashMap<>();
        // THIS IS FOR THE SELECTED SHAPE
        DropShadow dropShadowEffect = new DropShadow();
        dropShadowEffect.setOffsetX(0.0f);
        dropShadowEffect.setOffsetY(0.0f);
        dropShadowEffect.setSpread(1.0);
        dropShadowEffect.setColor(Color.YELLOW);
        dropShadowEffect.setBlurType(BlurType.GAUSSIAN);
        dropShadowEffect.setRadius(15);
        highlightedEffect = dropShadowEffect;
        backgroundImage = null;
    }

//    public int getMetroLineIndex() {
//        return metroLineIndex;
//    }
//    public HashMap<String, Line> getLines(){
//        return lines;
//    }
//    public HashMap<String, DraggableEllipse> getStations(){
//        return stations;
//    }
//    public HashMap<MetroLine, ArrayList<DraggableEllipse>> getConnections() {
//        return connections;
//    }
    public ArrayList<MetroLine> getLines() {
        return lines;
    }

    public ObservableList<Node> getNodes() {
        return nodes;
    }

    public void setNodes(ObservableList<Node> initNodes) {
        nodes = initNodes;
    }

    public void removeSelectedNode() {
        if (selectedNode != null) {
            nodes.remove(selectedNode);
            selectedNode = null;
        }
    }

//    public void fillConnections() {
//        for (Node node : nodes) {
//            if (node instanceof MetroLine) {
//                connections.put((MetroLine) node, new ArrayList<DraggableEllipse>());
//            } else if (node instanceof DraggableEllipse) {
//                
//            }
//        }
//    }
    /**
     * This function clears out the HTML tree and reloads it with the minimal
     * tags, like html, head, and body such that the user can begin editing a
     * page.
     */
    @Override
    public void resetData() {
        setState(MMMState.SELECTING_NODE);
        newShape = null;
        selectedNode = null;
// INIT THE COLORS
        currentFillColor = Color.web(WHITE_HEX);
        currentOutlineColor = Color.web(BLACK_HEX);
        lines.clear();
        nodes.clear();
        //connections.clear();
        ((MMMWorkspace) app.getWorkspaceComponent()).getBackgroundColorPicker().setValue(Color.web(WHITE_HEX));
        ((MMMWorkspace) app.getWorkspaceComponent()).getElement().getChildren().clear();
        ((MMMWorkspace) app.getWorkspaceComponent()).getGrid().setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));

    }

    public ImageView getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(ImageView i) {
        backgroundImage = i;
        Pane grid = ((MMMWorkspace) app.getWorkspaceComponent()).getGrid();
        if (grid.getChildren().get(0) instanceof ImageView) {
            grid.getChildren().remove(0);
        }
        grid.getChildren().add(0, i);
    }

    public Color getBackgroundColor() {
        return (Color) ((MMMWorkspace) app.getWorkspaceComponent()).getGrid().getBackground().getFills().get(0).getFill();
    }

    public void setBackgroundColor(Color color) {
        Pane grid = ((MMMWorkspace) app.getWorkspaceComponent()).getGrid();
        BackgroundFill fill = new BackgroundFill(color, null, null);
        Background background = new Background(fill);
        grid.setBackground(background);
    }

//    public void selectSizedShape() {
//        if (selectedNode != null) {
//            unhighlightNode(selectedNode);
//        }
//        selectedNode = newShape;
//        highlightNode(selectedNode);
//        newShape = null;
//        if (state == SIZING_SHAPE) {
//            state = ((Draggable) selectedNode).getStartingState();
//        }
//    }
    public void unhighlightNode(Node node) {
        node.setEffect(null);
    }

    public void highlightNode(Node node) {
        node.setEffect(highlightedEffect);
    }

    public Shape getNewShape() {
        return newShape;
    }

    public Node getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(Node initSelectedNode) {
        selectedNode = initSelectedNode;
    }

    public Node selectTopNode(int x, int y) {
        Node node = getTopNode(x, y);
        if (node == selectedNode) {
            return node;
        }

        if (selectedNode != null) {
            unhighlightNode(selectedNode);
        }
        if (node != null) {
            highlightNode(node);
            MMMWorkspace workspace = (MMMWorkspace) app.getWorkspaceComponent();
            workspace.loadSelectedNodeSettings(node);
        }
        selectedNode = node;
//        if (node != null) {
//            ((Draggable) node).setStart(x, y);
//        }
        return node;
    }

    public boolean isShape(Draggable node) {
        return ((node.getNodeType() == Draggable.ELLIPSE)
                || (node.getNodeType() == Draggable.TEXT));
    }

    public Draggable getSelectedDraggableNode() {
        if (selectedNode == null) {
            return null;
        } else {
            return (Draggable) selectedNode;
        }
    }

    public Node getTopNode(int x, int y) {
        for (int i = nodes.size() - 1; i >= 0; i--) {
            Node node = (Node) nodes.get(i);
            if (node.contains(x, y)) {
                return node;
            }
        }
        return null;
    }

    public MMMState getState() {
        return state;
    }

    public void setState(MMMState initState) {
        state = initState;
    }

    public boolean isInState(MMMState testState) {
        return state == testState;
    }

    // METHODS NEEDED BY TRANSACTIONS
    public void moveNodeToIndex(Node nodeToMove, int index) {
        int currentIndex = nodes.indexOf(nodeToMove);
        int numberOfNodes = nodes.size();
        if ((currentIndex >= 0) && (index >= 0) && (index < numberOfNodes)) {
            // IS IT SUPPOSED TO BE THE LAST ONE?
            if (index == (numberOfNodes - 1)) {
                nodes.remove(currentIndex);
                nodes.add(nodeToMove);
            } else {
                nodes.remove(currentIndex);
                nodes.add(index, nodeToMove);
            }
        }
    }

    public void removeNode(Node nodeToRemove) {
        int currentIndex = nodes.indexOf(nodeToRemove);
        if (currentIndex >= 0) {
            nodes.remove(currentIndex);
        }
    }

    public void addElement(Node nodeToAdd) {
        int currentIndex = nodes.indexOf(nodeToAdd);
        if (currentIndex < 0) {
            nodes.add(nodeToAdd);
        }
    }

    public void addLine(MetroLine line) {
        int currentIndex = nodes.indexOf(line);
        if (currentIndex < 0) {
            //metroLineIndex++;
            lines.add(line);
            DraggableText leftText = new DraggableText(line.getId());
            leftText.setFill((Color) line.getStroke());
            leftText.xProperty().bind(line.getLineSegments().get(0).startXProperty().subtract(leftText.getLayoutBounds().getWidth()));
            leftText.yProperty().bind(line.getLineSegments().get(0).startYProperty());
            leftText.setTextAlignment(TextAlignment.RIGHT);
            DraggableText rightText = new DraggableText(line.getId());
            rightText.xProperty().bind(line.getLineSegments().get(line.getLineSegments().size() - 1).endXProperty());
            rightText.yProperty().bind(line.getLineSegments().get(line.getLineSegments().size() - 1).endYProperty());
            rightText.setFill((Color) line.getStroke());
            rightText.setTextAlignment(TextAlignment.LEFT);
            nodes.addAll(leftText, rightText);
        }
    }

    public void removeLine(ArrayList<Line> lineSegs) {
        int currentIndex = nodes.indexOf(lineSegs.get(0));
        if (currentIndex >= 0) {
            // REMOVE LINE SEGMENTS FROM NODES
            for (Line line : lineSegs) {
                nodes.remove(line);
            }
            // REMOVE METRO LINE FROM LINES
            for (MetroLine line : lines) {
                if (line.getId().equals(lineSegs.get(0).getId())) {
                    lines.remove(line);
                    break;
                }
            }
            // REMOVE LINE LABELS
            int indexToRemove = -1;
            for (Node node : nodes) {
                if (node instanceof DraggableText && ((DraggableText) node).getText().equals(lineSegs.get(0).getId())) {
                    indexToRemove = nodes.indexOf(node);
                    break;
                }
            }
            nodes.remove(indexToRemove, indexToRemove + 2);
            //metroLineIndex--;
            //connections.remove(line);

        }
    }

    public void addNewStation(DraggableEllipse station) {
        int currentIndex = nodes.indexOf(station);
        if (currentIndex < 0) {
            nodes.add(station);
            DraggableText stationLabel = new DraggableText(station.getId());
            stationLabel.xProperty().bind(station.centerXProperty().add(station.radiusXProperty()));
            stationLabel.yProperty().bind(station.centerYProperty().subtract(station.radiusYProperty()));
            //stationLabel.start((int)(station.getCenterX() + station.getRadiusX()), (int)(station.getCenterY() - station.getRadiusY()));
            //stationLabel.setX(station.getCenterX() + station.getRadiusX());
            //stationLabel.setY(station.getCenterY() - station.getRadiusY());
            nodes.add(stationLabel);
        }
    }

    public void addStation(DraggableEllipse station) {

    }

    public void removeStation(DraggableEllipse station, DraggableText label) {
        int currentIndex = nodes.indexOf(station);
        if (currentIndex >= 0) {
            // IF STATION IS NOT ON THE LINE
            if (station.getLines().size() == 0) {
                // REMOVE STATION AND LABEL FROM MAP
                unhighlightNode(station);
                nodes.removeAll(station, label);
                setSelectedNode(null);
            } // IF STATION IS ON THE LINE
            else {
                // FOR EACH STATION'S LINE
                int numOfMetroLine = 0;
                while (numOfMetroLine < station.getLines().size()) {
                    // FIND THE STATION'S LINE
                    for (MetroLine ml : lines) {
                        int index = ml.getStations().indexOf(station);
                        if (ml.getId().equals(station.getLines().get(numOfMetroLine))) {
                             // CHECK THE STATION IS ON THE RIGHT END OF THE METRO LINE
                             if (ml.getStations().indexOf(station) == ml.getStations().size() - 1) {
                                double oldendx = ml.getLineSegments().get(index + 1).getEndX();
                                double oldendy = ml.getLineSegments().get(index + 1).getEndY();
                                // REMOVE RIGHT  LINE
                                nodes.remove(ml.getLineSegments().get(index + 1));
                                ml.getLineSegments().remove(index + 1);
                                // FIND THE RIGHT LABEL
                                DraggableText rightLabel = null;
                                for (Node n : nodes) {
                                    if (n instanceof DraggableText && ((DraggableText) n).getText().equals(ml.getId()) && ((DraggableText) n).getTextAlignment().equals(TextAlignment.LEFT)) {
                                        rightLabel = (DraggableText) n;
                                        break;
                                    }
                                }
                                // RELOCATE LEFT LINE
                                ml.getLineSegments().get(index).endXProperty().unbind();
                                ml.getLineSegments().get(index).endYProperty().unbind();
                                ml.getLineSegments().get(index).endXProperty().set(oldendx);
                                ml.getLineSegments().get(index).endYProperty().set(oldendy);
                                rightLabel.xProperty().bind(ml.getLineSegments().get(index).endXProperty());
                                rightLabel.yProperty().bind(ml.getLineSegments().get(index).endYProperty());
                            } 
                             // CHECK THE STATION IS ON THE LEFT END OR THE MID OF THE METRO LINE 
                            else {
                                ml.getLineSegments().get(index).endXProperty().bind(ml.getStations().get(index + 1).centerXProperty());
                                ml.getLineSegments().get(index).endYProperty().bind(ml.getStations().get(index + 1).centerYProperty());
                                // REMOVE RIGHT  LINE
                                nodes.remove(ml.getLineSegments().get(index + 1));
                                ml.getLineSegments().remove(index + 1);
                            }
                            // REMOVE STATION
                            ml.removeStationName(station.getId());
                            ml.removeStation(station);
                            nodes.remove(station);
                            // REMOVE STATION LABEL
                            for (Node node : nodes) {
                                if (node instanceof DraggableText && ((DraggableText) node).getText().equals(station.getId())) {
                                    nodes.remove(node);
                                    break;
                                }
                            }
                        }
                    }
                    numOfMetroLine++;
//                    // FIND THE X,Y COORDINATE
//                    for (Line l : lines.get(indexOfMetroLineFromLines).getLineSegments()) {
//                        // FIND THE LEFT RIGHT SEGMENT OF THE STATION
//                        if (l.getStartX() == station.getCenterX() && l.getStartY() == station.getCenterY()) {
//                            //EXPAND THE LEFT LINE SEGMENT
//                            int leftIndex = lines.indexOf(l) - 1;
//                            lines.get(leftIndex).setEndX(l.getEndX());
//                            lines.get(leftIndex).setEndY(l.getEndY());
//                            // REMOVE THE RIGHT LINE SEGMENT
//                            lines.remove(l);
//                            break;
//                        }
//                    }
                }
            }
        }
    }

    public int getIndexOfNode(Node node) {
        return nodes.indexOf(node);
    }

    public void addNodeAtIndex(Node node, int nodeIndex) {
        nodes.add(nodeIndex, node);
    }

    public boolean isTextSelected() {
        if (selectedNode == null) {
            return false;
        } else {
            return (selectedNode instanceof Text);
        }
    }

    public void setMetroLineIndex(int i) {
        //metroLineIndex = i;
    }
}
