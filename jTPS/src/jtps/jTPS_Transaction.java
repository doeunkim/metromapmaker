package jtps;

/**
 *
 * @author  Doeun Kim
 */
public interface jTPS_Transaction {
    public void doTransaction();
    public void undoTransaction();
}
